package cz.esw.serialization.handler;

import cz.esw.serialization.ResultConsumer;
import cz.esw.serialization.avro.*;
import cz.esw.serialization.avro.AInput.Builder;
import cz.esw.serialization.json.DataType;
import cz.esw.serialization.json.Dataset;
import cz.esw.serialization.json.MeasurementInfo;
import org.apache.avro.io.*;
import org.apache.avro.specific.SpecificDatumReader;
import org.apache.avro.specific.SpecificDatumWriter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.*;

/**
 * @author Marek Cuchý (CVUT)
 */
public class AvroDataHandler implements DataHandler {

    private final InputStream is;
    private final OutputStream os;

    protected Map<Integer, Dataset> datasets;

    /**
     * @param is input stream from which the results will be read
     * @param os output stream to which the data have to written
     */
    public AvroDataHandler(InputStream is, OutputStream os) {
        this.is = is;
        this.os = os;
    }

	@Override
	public void start() {
        datasets = new HashMap<>();
	}

	@Override
	public void handleNewDataset(int datasetId, long timestamp, String measurerName) {
        Dataset dataset = new Dataset();
        dataset.setInfo(new MeasurementInfo(datasetId, timestamp, measurerName));
        dataset.setRecords(new EnumMap<>(DataType.class));
        datasets.put(datasetId, dataset);
	}

	@Override
	public void handleValue(int datasetId, DataType type, double value) {
        Dataset dataset = datasets.get(datasetId);
        if (dataset == null) {
            throw new IllegalArgumentException("Dataset with id " + datasetId + " not initialized.");
        }
        dataset.getRecords().computeIfAbsent(type, t -> new ArrayList<>()).add(value);
	}

	@Override
    public void getResults(ResultConsumer consumer) throws IOException {
        //TODO jen tady udelat prevod na ty proto objekty z datasets shitu

        Builder input = AInput.newBuilder();
        List<ADataset> aDatasets = new ArrayList<>();
        datasets.forEach((k, v) -> {
            AMeasurementInfo info = AMeasurementInfo.newBuilder()
                    .setId(v.getInfo().getId())
                    .setTimestamp(v.getInfo().getTimestamp())
                    .setMeasurerName(v.getInfo().getMeasurerName())
                    .build();

            Map<CharSequence, AListOfDoubles> records = new HashMap<>();
            v.getRecords().keySet().forEach(kk -> {
                AListOfDoubles listOfDoubles = AListOfDoubles.newBuilder()
                        .setNumber(v.getRecords().get(kk))
                        .build();
                records.put(kk.toString(), listOfDoubles);
            });

            ADataset dataset = ADataset.newBuilder()
                    .setInfo(info)
                    .setRecords(records)
                    .build();

            aDatasets.add(dataset);
        });

        AInput res = input.setDatasets(aDatasets).build();

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

        //TODO Tady nevim jestli handluji spravne ty zapisy
        DatumReader<AOutput> reader = new SpecificDatumReader<>(AOutput.getClassSchema());
        DatumWriter<AInput> writer = new SpecificDatumWriter<>(AInput.getClassSchema());

        BinaryDecoder binaryDecoder = DecoderFactory.get().binaryDecoder(is, null);
        BinaryEncoder encoder = EncoderFactory.get().binaryEncoder(byteArrayOutputStream, null);
        writer.write(res, encoder);
        encoder.flush();

        byte[] bytes = byteArrayOutputStream.toByteArray();

        int size = bytes.length;
        String sizeString = String.valueOf(size);
        os.write(sizeString.getBytes());

        StringBuilder zeros = new StringBuilder();
        for (int i = 0; i < 10 - sizeString.length(); i++) {
            //zeros.append("O");
            System.out.println("0");
            os.write(0);
        }

        os.write(bytes);
        os.flush();

        //TODO nevim jestli potreba???
        //os.write(0); //write end

        // wait!
        while (!(is.available() == 0));

        reader.read(null, binaryDecoder).getResults().forEach(r -> {
            AMeasurementInfo info = r.getInfo();
            consumer.acceptMeasurementInfo(info.getId(), info.getTimestamp(), info.getMeasurerName().toString());
            r.getAverages().forEach((k, v) -> consumer.acceptResult(DataType.valueOf(k.toString()), v));
        });


	}
}

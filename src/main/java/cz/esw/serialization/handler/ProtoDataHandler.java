package cz.esw.serialization.handler;

import cz.esw.serialization.ResultConsumer;
import cz.esw.serialization.json.DataType;
import cz.esw.serialization.json.Dataset;
import cz.esw.serialization.json.MeasurementInfo;
import cz.esw.serialization.proto.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;

/**
 * V principu jsem prekopiroval strukturu, kterou pouziva Cuchy a potom jsem ji jen prevedl na proto
 *
 * @author Marek Cuchý (CVUT)
 */
public class ProtoDataHandler implements DataHandler {

    private final InputStream is;
    private final OutputStream os;

    protected Map<Integer, Dataset> datasets;

    /**
     * @param is input stream from which the results will be read
     * @param os output stream to which the data have to written
     */
    public ProtoDataHandler(InputStream is, OutputStream os) {
        this.is = is;
        this.os = os;
    }

    @Override
    public void start() {
        datasets = new HashMap<>();
    }

    @Override
    public void handleNewDataset(int datasetId, long timestamp, String measurerName) {
        Dataset dataset = new Dataset();
        dataset.setInfo(new MeasurementInfo(datasetId, timestamp, measurerName));
        dataset.setRecords(new EnumMap<>(DataType.class));
        datasets.put(datasetId, dataset);
    }

    @Override
    public void handleValue(int datasetId, DataType type, double value) {
        Dataset dataset = datasets.get(datasetId);
        if (dataset == null) {
            throw new IllegalArgumentException("Dataset with id " + datasetId + " not initialized.");
        }
        dataset.getRecords().computeIfAbsent(type, t -> new ArrayList<>()).add(value);
    }

    @Override
    public void getResults(ResultConsumer consumer) throws IOException {
        //TODO jen tady udelat prevod na ty proto objekty z datasets shitu
        PInput.Builder input = PInput.newBuilder();

        datasets.forEach((k, v) -> {
            PMeasurementInfo info = PMeasurementInfo.newBuilder()
                    .setId(v.getInfo().getId())
                    .setTimestamp(v.getInfo().getTimestamp())
                    .setMeasurerName(v.getInfo().getMeasurerName())
                    .build();

            Map<String, PListOfDoubles> records = new HashMap<>();
            v.getRecords().keySet().forEach(kk -> {
                PListOfDoubles listOfDoubles = PListOfDoubles.newBuilder()
                        .addAllNumber(v.getRecords().get(kk))
                        .build();
                records.put(kk.toString(), listOfDoubles);
            });

            PDataset dataset = PDataset.newBuilder()
                    .setInfo(info)
                    .putAllRecords(records)
                    .build();

            input.addDatasets(dataset);
        });

        PInput res = input.build();

        int size = res.getSerializedSize();
        String sizeString = String.valueOf(size);
        os.write(sizeString.getBytes());

        StringBuilder zeros = new StringBuilder();
        for (int i = 0; i < 10 - sizeString.length(); i++) {
            //zeros.append("O");
            System.out.println("0");
            os.write(0);
        }

        //System.out.println(bytes);
        System.out.println(res.getSerializedSize());

        input.build().writeTo(os);
        // os.write(0); //write end

        POutput result = POutput.parseFrom(is);

        result.getResultsList().forEach(r -> {
            PMeasurementInfo info = r.getInfo();
            consumer.acceptMeasurementInfo((int) info.getId(), info.getTimestamp(), info.getMeasurerName());
            r.getAveragesMap().forEach((k, v) ->
                    consumer.acceptResult(DataType.valueOf(k), v)
            );
        });
    }
}

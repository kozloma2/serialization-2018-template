#include <iostream>

#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <cstring>
#include <unistd.h>


#include <jsoncpp/json/json.h>


#include "measurementinfo.h"
#include "dataset.h"
#include "result.h"
#include "measurements.pb.h"
#include "avro_measurements.hh"

//#include <boost/array.hpp>
#include <boost/asio.hpp>
#include <tiff.h>
#include <fstream>

using namespace std;
using boost::asio::ip::tcp;

void processProtobuf(tcp::iostream& stream);

void processJSON(tcp::iostream& stream){
    Json::Value val;
    Json::Reader reader;

    std::vector<esw::PDataset> p_datasets;


    std::vector<Dataset> datasets;
    std::vector<esw::PDataset> proto_datasets;
    std::vector<Result> results;

    /* Read json string from the stream */
    string s;
    getline(stream, s, '\0');

    /* Parse string */
    reader.parse(s, val);

    cout << val;

    datasets.clear();
    results.clear();
    for (int i = 0; i < val.size(); i++) {
        datasets.emplace_back();
        datasets[i].Deserialize(val[i]);
        /* Calculate averages */
        results.emplace_back(datasets[i].getInfo(), datasets[i].getRecords());
    }

    /* Create output JSON structure */
    Json::Value out;
//    Json::FastWriter writer;
    Json::StyledWriter writer;
    for (int i = 0; i < results.size(); i++) {
        Json::Value result;
        results[i].Serialize(result);
        out[i] = result;
    }

    /* Send the result back */
    std::string output = writer.write(out);
    stream << output;
    cout << output;
}

void processAvro(tcp::iostream& stream){

    char * initial_size_message = new char[10];

    stream.read(initial_size_message, 10);

    int message_size = std::stoi(initial_size_message);

    cout << message_size << endl;

    char * message = new char[message_size];
    bzero(message, message_size);

    stream.read(message, message_size);

    avro::DecoderPtr decoder = avro::binaryDecoder();
    std::unique_ptr<avro::InputStream> input_stream = avro::memoryInputStream((const uint8_t *)message, message_size);

    decoder->init(*input_stream);

    esw::AInput input;
    avro::decode(*decoder, input);

    esw::AOutput output;

    int i = 0;

    for (auto& new_ds : input.datasets) {

/*
        cout << "jedem " << endl;
        cout << new_ds.info.id << endl;
        cout << new_ds.info.timestamp << endl;

        for (auto &pair : new_ds.records) {
            cout << pair.first << endl;
            for (int i = 0; i < pair.second.number.size(); i++) {
                cout << " " << pair.second.number[i] << endl;
            }
        }
*/

        esw::AResult result = esw::AResult();
        result.info = new_ds.info;

        auto &averages = result.averages;

        for (auto it = new_ds.records.begin(); it != new_ds.records.end(); ++it){

            double average = 0;
            for(auto val = it-> second.number.begin(); val != it->second.number.end(); ++val){
                average += *val;
            }
            averages[it->first] = (average / it->second.number.size());
        }

        //cout << i << endl;
        output.results.push_back(result);
        i++;
    }


    std::unique_ptr<avro::OutputStream> output_stream = avro::ostreamOutputStream(stream);

    cout << "doing fine" << endl;
    //cout << i << endl;

    avro::EncoderPtr encoder = avro::binaryEncoder();
    encoder->init(*output_stream);

    avro::encode(*encoder, output);


    output_stream->flush();
}

void createProtoData() {

    esw::PMeasurementInfo * info = new esw::PMeasurementInfo();
    info->set_id(32055653);
    info->set_allocated_measurername(new string("michal"));
    info->set_timestamp(1556608275825);

    esw::PListOfDoubles *list = new esw::PListOfDoubles();
    list->add_number(1);
    list->add_number(2);
    esw::PListOfDoubles *list1 = new esw::PListOfDoubles();
    list1->add_number(3);
    list1->add_number(4);
    esw::PListOfDoubles *list2 = new esw::PListOfDoubles();
    list2->add_number(5);
    list2->add_number(6);

    // SECOND INFO AND THE LISTS

    esw::PMeasurementInfo * info1 = new esw::PMeasurementInfo();
    info1->set_id(111111111);
    info1->set_allocated_measurername(new string("ondra"));
    info1->set_timestamp(1231121825);

    esw::PListOfDoubles *list3 = new esw::PListOfDoubles();
    list3->add_number(11);
    list3->add_number(12);
    esw::PListOfDoubles *list4 = new esw::PListOfDoubles();
    list4->add_number(13);
    list4->add_number(14);
    esw::PListOfDoubles *list5 = new esw::PListOfDoubles();
    list5->add_number(15);
    list5->add_number(16);

    // THE WHOLE INPUT PUT TOGETHER

    esw::PInput * input = new esw::PInput();
    esw::PDataset * dataset2 = input->add_datasets();
    dataset2->set_allocated_info(info);

    auto &map2 = *(dataset2->mutable_records());
    map2["DOWNLOAD"] = *(list);
    map2["UPLOAD"] = *(list1);
    map2["PING"] = *(list2);

    esw::PDataset * dataset3 = input->add_datasets();
    dataset3->set_allocated_info(info1);

    auto &map3 = *(dataset3->mutable_records());
    map3["DOWNLOAD"] = *(list3);
    map3["UPLOAD"] = *(list4);
    map3["PING"] = *(list5);

    cout << "JEDEM" << endl;

    esw::PInput in = *(input);
    string s = in.SerializeAsString();

//    cout << s << endl;

    ofstream myfile;
    myfile.open ("example.txt");
    myfile << s.size() << s;
    myfile.close();
}

void processProtobuf(tcp::iostream& stream){
    //throw std::logic_error("TODO: Implement profobuf");
    GOOGLE_PROTOBUF_VERIFY_VERSION;

    char * initial_size_message = new char[10];

    stream.read(initial_size_message, 10);

    int message_size = std::stoi(initial_size_message);

    cout << message_size << endl;

    char * message = new char[message_size];
    bzero(message, message_size);

    stream.read(message, message_size);

    esw::PInput new_input;
    new_input.ParseFromArray(message, message_size);


    esw::POutput * output = new esw::POutput();

    for (auto new_ds : new_input.datasets()) {

        /*cout << "jedem " << endl;
        cout << new_ds.info().id() << endl;
        cout << new_ds.info().timestamp() << endl;

        for (auto &pair : new_ds.records()) {
            cout << pair.first << endl;
            for (int i = 0; i < pair.second.number_size(); i++) {
                cout << " " << pair.second.number(i) << endl;
            }
        }
*/
        esw::PResult * result = output->add_results();

        esw::PMeasurementInfo * info = new esw::PMeasurementInfo();
        info->CopyFrom(new_ds.info());
        result->set_allocated_info(info);

        auto &averages = *(result->mutable_averages());

        for (auto it = new_ds.records().begin(); it != new_ds.records().end(); ++it){

            double average = 0;
            for(auto val = it-> second.number().begin(); val != it->second.number().end(); ++val){
                average += *val;
            }
            averages[it->first] = (average/it->second.number_size());
/*
            for (auto &pair : *(result->mutable_averages())) {
                cout << "first" << endl;
                cout << " " << pair.first << endl;
                cout << " " << pair.second << endl;

            }
*/
        }
/*
        cout << result->info().id();
        cout << result->info().measurername();
        cout << result->info().timestamp();
        */
    }

    output->SerializeToOstream(&stream);
//    cout << output_string;
//    stream << output_string;

}

int main(int argc, char *argv[]) {

    if (argc != 3) {
        cout << "Error: two arguments required - ./server  <port> <protocol>" << endl;
        return 1;
    }

    //createProtoData();

    // unsigned short int port = 12345;
    unsigned short int port = atoi(argv[1]);

    // std::string protocol = "json";
    std::string protocol(argv[2]);
    try {
        boost::asio::io_service io_service;

        tcp::endpoint endpoint(tcp::v4(), port);
        tcp::acceptor acceptor(io_service, endpoint);

        while (true) {
            tcp::iostream stream;
            boost::system::error_code ec;
            acceptor.accept(*stream.rdbuf(), ec);

            if(protocol == "json"){
                processJSON(stream);
            }else if(protocol == "avro"){
                processAvro(stream);
            }else if(protocol == "proto"){
                processProtobuf(stream);
            }else{
                throw std::logic_error("Protocol not yet implemented");
            }

        }

    }
    catch (std::exception &e) {
        std::cerr << e.what() << std::endl;
    }

    return 0;
}

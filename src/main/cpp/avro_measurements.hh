/**
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


#ifndef MEASUREMENTS_HH_210278647__H_
#define MEASUREMENTS_HH_210278647__H_


#include <sstream>
#include "boost/any.hpp"
#include "avro/Specific.hh"
#include "avro/Encoder.hh"
#include "avro/Decoder.hh"

namespace esw {
struct AMeasurementInfo {
    int32_t id;
    int64_t timestamp;
    std::string measurerName;
    AMeasurementInfo() :
        id(int32_t()),
        timestamp(int64_t()),
        measurerName(std::string())
        { }
};

struct AListOfDoubles {
    std::vector<double > number;
    AListOfDoubles() :
        number(std::vector<double >())
        { }
};

struct ADataset {
    AMeasurementInfo info;
    std::map<std::string, AListOfDoubles > records;
    ADataset() :
        info(AMeasurementInfo()),
        records(std::map<std::string, AListOfDoubles >())
        { }
};

struct AResult {
    AMeasurementInfo info;
    std::map<std::string, double > averages;
    AResult() :
        info(AMeasurementInfo()),
        averages(std::map<std::string, double >())
        { }
};

struct AInput {
    std::vector<ADataset > datasets;
    AInput() :
        datasets(std::vector<ADataset >())
        { }
};

struct AOutput {
    std::vector<AResult > results;
    AOutput() :
        results(std::vector<AResult >())
        { }
};

struct measurements_json_Union__0__ {
private:
    size_t idx_;
    boost::any value_;
public:
    size_t idx() const { return idx_; }
    AMeasurementInfo get_AMeasurementInfo() const;
    void set_AMeasurementInfo(const AMeasurementInfo& v);
    AListOfDoubles get_AListOfDoubles() const;
    void set_AListOfDoubles(const AListOfDoubles& v);
    ADataset get_ADataset() const;
    void set_ADataset(const ADataset& v);
    AResult get_AResult() const;
    void set_AResult(const AResult& v);
    AInput get_AInput() const;
    void set_AInput(const AInput& v);
    AOutput get_AOutput() const;
    void set_AOutput(const AOutput& v);
    measurements_json_Union__0__();
};

inline
AMeasurementInfo measurements_json_Union__0__::get_AMeasurementInfo() const {
    if (idx_ != 0) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<AMeasurementInfo >(value_);
}

inline
void measurements_json_Union__0__::set_AMeasurementInfo(const AMeasurementInfo& v) {
    idx_ = 0;
    value_ = v;
}

inline
AListOfDoubles measurements_json_Union__0__::get_AListOfDoubles() const {
    if (idx_ != 1) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<AListOfDoubles >(value_);
}

inline
void measurements_json_Union__0__::set_AListOfDoubles(const AListOfDoubles& v) {
    idx_ = 1;
    value_ = v;
}

inline
ADataset measurements_json_Union__0__::get_ADataset() const {
    if (idx_ != 2) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<ADataset >(value_);
}

inline
void measurements_json_Union__0__::set_ADataset(const ADataset& v) {
    idx_ = 2;
    value_ = v;
}

inline
AResult measurements_json_Union__0__::get_AResult() const {
    if (idx_ != 3) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<AResult >(value_);
}

inline
void measurements_json_Union__0__::set_AResult(const AResult& v) {
    idx_ = 3;
    value_ = v;
}

inline
AInput measurements_json_Union__0__::get_AInput() const {
    if (idx_ != 4) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<AInput >(value_);
}

inline
void measurements_json_Union__0__::set_AInput(const AInput& v) {
    idx_ = 4;
    value_ = v;
}

inline
AOutput measurements_json_Union__0__::get_AOutput() const {
    if (idx_ != 5) {
        throw avro::Exception("Invalid type for union");
    }
    return boost::any_cast<AOutput >(value_);
}

inline
void measurements_json_Union__0__::set_AOutput(const AOutput& v) {
    idx_ = 5;
    value_ = v;
}

inline measurements_json_Union__0__::measurements_json_Union__0__() : idx_(0), value_(AMeasurementInfo()) { }
}
namespace avro {
template<> struct codec_traits<esw::AMeasurementInfo> {
    static void encode(Encoder& e, const esw::AMeasurementInfo& v) {
        avro::encode(e, v.id);
        avro::encode(e, v.timestamp);
        avro::encode(e, v.measurerName);
    }
    static void decode(Decoder& d, esw::AMeasurementInfo& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.id);
                    break;
                case 1:
                    avro::decode(d, v.timestamp);
                    break;
                case 2:
                    avro::decode(d, v.measurerName);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.id);
            avro::decode(d, v.timestamp);
            avro::decode(d, v.measurerName);
        }
    }
};

template<> struct codec_traits<esw::AListOfDoubles> {
    static void encode(Encoder& e, const esw::AListOfDoubles& v) {
        avro::encode(e, v.number);
    }
    static void decode(Decoder& d, esw::AListOfDoubles& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.number);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.number);
        }
    }
};

template<> struct codec_traits<esw::ADataset> {
    static void encode(Encoder& e, const esw::ADataset& v) {
        avro::encode(e, v.info);
        avro::encode(e, v.records);
    }
    static void decode(Decoder& d, esw::ADataset& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.info);
                    break;
                case 1:
                    avro::decode(d, v.records);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.info);
            avro::decode(d, v.records);
        }
    }
};

template<> struct codec_traits<esw::AResult> {
    static void encode(Encoder& e, const esw::AResult& v) {
        avro::encode(e, v.info);
        avro::encode(e, v.averages);
    }
    static void decode(Decoder& d, esw::AResult& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.info);
                    break;
                case 1:
                    avro::decode(d, v.averages);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.info);
            avro::decode(d, v.averages);
        }
    }
};

template<> struct codec_traits<esw::AInput> {
    static void encode(Encoder& e, const esw::AInput& v) {
        avro::encode(e, v.datasets);
    }
    static void decode(Decoder& d, esw::AInput& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.datasets);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.datasets);
        }
    }
};

template<> struct codec_traits<esw::AOutput> {
    static void encode(Encoder& e, const esw::AOutput& v) {
        avro::encode(e, v.results);
    }
    static void decode(Decoder& d, esw::AOutput& v) {
        if (avro::ResolvingDecoder *rd =
            dynamic_cast<avro::ResolvingDecoder *>(&d)) {
            const std::vector<size_t> fo = rd->fieldOrder();
            for (std::vector<size_t>::const_iterator it = fo.begin();
                it != fo.end(); ++it) {
                switch (*it) {
                case 0:
                    avro::decode(d, v.results);
                    break;
                default:
                    break;
                }
            }
        } else {
            avro::decode(d, v.results);
        }
    }
};

template<> struct codec_traits<esw::measurements_json_Union__0__> {
    static void encode(Encoder& e, esw::measurements_json_Union__0__ v) {
        e.encodeUnionIndex(v.idx());
        switch (v.idx()) {
        case 0:
            avro::encode(e, v.get_AMeasurementInfo());
            break;
        case 1:
            avro::encode(e, v.get_AListOfDoubles());
            break;
        case 2:
            avro::encode(e, v.get_ADataset());
            break;
        case 3:
            avro::encode(e, v.get_AResult());
            break;
        case 4:
            avro::encode(e, v.get_AInput());
            break;
        case 5:
            avro::encode(e, v.get_AOutput());
            break;
        }
    }
    static void decode(Decoder& d, esw::measurements_json_Union__0__& v) {
        size_t n = d.decodeUnionIndex();
        if (n >= 6) { throw avro::Exception("Union index too big"); }
        switch (n) {
        case 0:
            {
                esw::AMeasurementInfo vv;
                avro::decode(d, vv);
                v.set_AMeasurementInfo(vv);
            }
            break;
        case 1:
            {
                esw::AListOfDoubles vv;
                avro::decode(d, vv);
                v.set_AListOfDoubles(vv);
            }
            break;
        case 2:
            {
                esw::ADataset vv;
                avro::decode(d, vv);
                v.set_ADataset(vv);
            }
            break;
        case 3:
            {
                esw::AResult vv;
                avro::decode(d, vv);
                v.set_AResult(vv);
            }
            break;
        case 4:
            {
                esw::AInput vv;
                avro::decode(d, vv);
                v.set_AInput(vv);
            }
            break;
        case 5:
            {
                esw::AOutput vv;
                avro::decode(d, vv);
                v.set_AOutput(vv);
            }
            break;
        }
    }
};

}
#endif

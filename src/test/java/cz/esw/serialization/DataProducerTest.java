package cz.esw.serialization;

import cz.esw.serialization.handler.AvroDataHandler;
import cz.esw.serialization.handler.JsonDataHandler;
import cz.esw.serialization.handler.ProtoDataHandler;
import cz.esw.serialization.json.Dataset;
import cz.esw.serialization.json.MeasurementInfo;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.Random;

/**
 * @author Marek Cuchý (CVUT)
 */
class DataProducerTest {

	@Test
	void generateDataAndCheckResultsJson() throws IOException {
		DataProducer producer = new DataProducer(new Random(0), 100, 10000);
		producer.generateDataAndCheckResults(new JsonHandler());
	}

	@Test
	void generateDataAndCheckResultsProto() throws IOException {
		DataProducer producer = new DataProducer(new Random(0), 100, 10000);
		producer.generateDataAndCheckResults(new ProtoHandler());
	}

    @Test
    void generateDataAndCheckResultsAvro() throws IOException {
        DataProducer producer = new DataProducer(new Random(0), 100, 10000);
        producer.generateDataAndCheckResults(new AvroHandler());
    }

	private static class JsonHandler extends JsonDataHandler {
		public JsonHandler() {
			super(null, null);
		}

		@Override
		public void getResults(ResultConsumer consumer) {
			for (Dataset dataset : datasets.values()) {
				MeasurementInfo info = dataset.getInfo();
				consumer.acceptMeasurementInfo(info.getId(), info.getTimestamp(), info.getMeasurerName());
				dataset.getRecords().forEach(
						(type, values) -> {
							consumer.acceptResult(type, values.stream().mapToDouble(Double::doubleValue).average().getAsDouble());
						}
				);
			}
		}

	}

	private static class ProtoHandler extends ProtoDataHandler {
		public ProtoHandler() {
			super(null, null);
		}

		@Override
		public void getResults(ResultConsumer consumer) {
			for (Dataset dataset : datasets.values()) {
				MeasurementInfo info = dataset.getInfo();
				consumer.acceptMeasurementInfo(info.getId(), info.getTimestamp(), info.getMeasurerName());
				dataset.getRecords().forEach(
						(type, values) -> {
							consumer.acceptResult(type, values.stream().mapToDouble(Double::doubleValue).average().getAsDouble());
						}
				);
			}
		}

	}

    private static class AvroHandler extends AvroDataHandler {
        public AvroHandler() {
            super(null, null);
        }

        @Override
        public void getResults(ResultConsumer consumer) {
            for (Dataset dataset : datasets.values()) {
                MeasurementInfo info = dataset.getInfo();
                consumer.acceptMeasurementInfo(info.getId(), info.getTimestamp(), info.getMeasurerName());
                dataset.getRecords().forEach(
                        (type, values) -> {
                            consumer.acceptResult(type, values.stream().mapToDouble(Double::doubleValue).average().getAsDouble());
                        }
                );
            }
        }

    }

}